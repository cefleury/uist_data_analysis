## R notebooks for analyzing data of Yiran's experiment for UIST 2020

You can access and edit the notebooks with Binder: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fcefleury%2Fuist_data_analysis.git/master)